#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os
import json


import jsonpatch
from tornado import websocket, web, ioloop, gen


from .storage import get_storage
from .swagger import (
    swagger_property,
    swagger_param,
    swagger_object,
    swagger_endpoint,
    swagger_document,
)


SwaggerInputPost = swagger_object(
    'InputPost',
    swagger_property('tag', str, example='foo'),
    swagger_property('text', str, example='Test #foo!'),
)
SwaggerPost = swagger_object(
    'Post',
    swagger_property('pid', str),
    swagger_property('tag', str),
    swagger_property('date', str),
    swagger_property('text', str),
)
SwaggerJSONPatchOperation = swagger_object(
    'JSONPatchOperation',
    swagger_property(
        'op', str,
        enum=['add', 'remove', 'replace', 'copy', 'move', 'test'],
        example='replace',
        description="Single JSON Patch (RFC 6902) operation",
    ),
    swagger_property(
        'path', str,
        example='/text',
        description="JSON Pointer (RFC 6901) expression",
    ),
    swagger_property('from?', str, example=''),
    swagger_property('value?', str, example='foo'),
)


storage = get_storage(os.environ.get(
    'TWITCHER_DATABASE', 'rethinkdb://admin:@localhost:28015/db'
))


class IndexHandler(web.RequestHandler):
    def get(self):
        self.render(
            "index.html",
            initial_data=storage.list(),
            selected_tag=None,
        )


class TagHandler(web.RequestHandler):
    def get(self, tag_name):
        self.render(
            "index.html",
            initial_data=storage.list(tag=tag_name),
            selected_tag=tag_name,
        )


class WebSocketManager(object):
    def __init__(self):
        self._handlers = {}

    def register(self, tag, handler):
        handler._twitcher_tag = tag
        if tag not in self._handlers:
            self._handlers[tag] = []
        if handler not in self._handlers[tag]:
            self._handlers[tag].append(handler)

    def unregister(self, handler):
        if handler in self._handlers.get(handler._twitcher_tag, []):
            self._handlers[handler._twitcher_tag].remove(handler)

    def broadcast(self, tag, data):
        for ws in self._handlers.get(tag, []):
            ws.write_message(data)

    def broadcast_insert(self, record):
        if record['tag']:
            self.broadcast(record['tag'], json.dumps(record))
        self.broadcast('', json.dumps(record))


websockets = WebSocketManager()


class SocketHandler(websocket.WebSocketHandler):
    def check_origin(self, origin):
        return True

    def open(self, tag_name):
        websockets.register(tag_name, self)

    def on_close(self):
        websockets.unregister(self)


class ApiHandler(web.RequestHandler):
    def respond(self, status_code, data=None):
        self.set_status(status_code)
        if data is not None:
            self.set_header('Content-Type', 'application/json')
            self.write(json.dumps(data, indent=4, sort_keys=True) + '\n')
        self.finish()

    def json_data(self):
        return json.loads(self.request.body.decode('utf-8'))

    def get_pagination(self):
        return {
            'before': self.get_argument('before', None),
            'limit': int(self.get_argument('limit', '10')),
        }


class ApiPostsHandler(ApiHandler):
    swagger_path = '/posts/'

    @swagger_endpoint(
        swagger_param('query', 'before?', str),
        swagger_param('query', 'limit?', int),
        swagger_param('response', '200', [SwaggerPost], description="Success"),
    )
    def get(self):
        """
        Return most recent posts
        """
        self.respond(200, storage.list(**self.get_pagination()))

    @swagger_endpoint(
        swagger_param(
            'body', 'body', SwaggerInputPost, description="Post to be stored",
        ),
        swagger_param(
            'response', '201', SwaggerPost, description="Success",
        ),
    )
    def post(self):
        """
        Adds new post
        """
        data = self.json_data()
        record = storage.add(
            tag=data.get('tag', ''), text=data.get('text', '')
        )
        self.respond(201, record)


class ApiPostHandler(ApiHandler):
    swagger_path = '/posts/{pid}'

    @swagger_endpoint(
        swagger_param(
            'path', 'pid', str, description="`pid` of posts to return"
        ),
        swagger_param('response', '200', SwaggerPost, description="Success"),
        swagger_param('response', '404', None, description="Missing"),
    )
    def get(self, pid):
        """
        Return single post by `pid`
        """
        result = storage.get(pid)
        if result is not None:
            self.respond(200, result)
        else:
            self.respond(404, result)

    @swagger_endpoint(
        swagger_param(
            'path', 'pid', str, description="`pid` of posts to update"
        ),
        swagger_param(
            'body', 'body', SwaggerInputPost, description="New post content",
        ),
        swagger_param('response', '200', SwaggerPost, description="Updated"),
        swagger_param('response', '404', None, description="Missing"),
    )
    def put(self, pid):
        """
        Update single post by `pid`
        """
        data = self.json_data()
        record = storage.update(
            pid=pid, tag=data.get('tag', ''), text=data.get('text', '')
        )
        if record is not None:
            self.respond(200, record)
        else:
            self.respond(404)

    @swagger_endpoint(
        swagger_param(
            'path', 'pid', str,
            description="`pid` of post to return to be updated",
        ),
        swagger_param('body', 'body', [SwaggerJSONPatchOperation]),
        swagger_param('response', '201', SwaggerPost, description="Updated"),
    )
    def patch(self, pid):
        """
        Partial note update
        Updates a note with sequence of JSON Patch
        ([RFC 6902](http://tools.ietf.org/html/rfc6902)) operation.
        For example:
        - `[{"op": "replace", "path": "/text", "value": "Hi!"}]` -
            set `text` value to "Hi!"
        """

        patch = jsonpatch.JsonPatch(self.json_data())

        record = storage.get(pid)
        result = patch.apply(record)
        record = storage.update(
            pid=pid, tag=result.get('tag', ''), text=result.get('text', '')
        )
        if record is not None:
            self.respond(200, record)
        else:
            self.respond(404)

    @swagger_endpoint(
        swagger_param(
            'path', 'pid', str, description="`pid` of posts to delete"
        ),
        swagger_param('response', '204', None, description="Deleted"),
        swagger_param('response', '404', None, description="Missing"),
    )
    def delete(self, pid):
        """
        Delete single post by `pid`
        """
        if storage.delete(pid):
            self.respond(204)
        else:
            self.respond(404)


class ApiTagsHandler(ApiHandler):
    swagger_path = '/posts/tagged/{tag}'

    @swagger_endpoint(
        swagger_param('query', 'before?', str),
        swagger_param('query', 'limit?', int),
        swagger_param(
            'path', 'tag', str, description="`tag` of posts to return"
        ),
        swagger_param('response', '200', [SwaggerPost], description="Success"),
    )
    def get(self, tag_name):
        """
        Return posts by tag
        """
        result = storage.list(tag=tag_name, **self.get_pagination())
        self.respond(200, result)


SWAGGER_DESCRIPTION = swagger_document(
    definitions=[
        SwaggerInputPost,
        SwaggerPost,
        SwaggerJSONPatchOperation,
    ],
    handlers=[ApiPostsHandler, ApiPostHandler, ApiTagsHandler],
    swagger='2.0',
    basePath='/api/v1',
    schemes=['http'],
    tags=[{'name': 'posts', 'description': "Posts' CRUD"}],
    info={
        'contact': {'email': "twitcher@kosyl.org"},
        'description': "Twitcher is the Killer App",
        'title': "Twitcher API",
        'version': "1.0.0",
    },
)


class SwaggerDescriptionHandler(ApiHandler):
    def get(self):
        self.respond(200, SWAGGER_DESCRIPTION)


app = web.Application([
    (r'/', IndexHandler),
    (r'/tag/(\w+)/?', TagHandler),

    (r'/ws/(\w*)', SocketHandler),

    (r'/api/v1/posts/?', ApiPostsHandler),
    (r'/api/v1/posts/(\d+)/?', ApiPostHandler),
    (r'/api/v1/posts/tagged/(\w+)/?', ApiTagsHandler),

    (r'/api/swagger/description.json', SwaggerDescriptionHandler),
    (r'/api/swagger', web.RedirectHandler,
        {'url': '/api/swagger/'}),
    (r'/api/swagger/()', web.StaticFileHandler,
        {'path': './static/swagger/', 'default_filename': 'index.html'}),
    (r'/api/swagger/(.+)', web.StaticFileHandler,
        {'path': './static/swagger/'}),
    (r'/(favicon.ico)', web.StaticFileHandler,
        {'path': './'}),
], autoreload=True)


def start_server():
    ioloop.IOLoop.current().add_callback(
        gen.coroutine(storage.get_changes(websockets.broadcast_insert))
    )
    print('starting on port', int(os.environ.get('TWITCHER_PORT', 8888)))
    app.listen(int(os.environ.get('TWITCHER_PORT', 8888)))
    ioloop.IOLoop.instance().start()
