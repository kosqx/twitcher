#!/usr/bin/env python
# -*- coding: utf-8 -*-


import time
import datetime
import random

try:
    from urllib.parse import urlparse
except ImportError:
    # Python 2
    from urlparse import urlparse


class RethinkdbStorage(object):
    def __init__(self, params):
        self._params = params
        import rethinkdb as r
        self.r = r
        self.conn = self.r.connect(**self._params)

        if params['db'] not in self.r.db_list().run(self.conn):
            self.r.db_create(params['db']).run(self.conn)
        if 'twitcher_posts' not in self.r.table_list().run(self.conn):
            self.r.table_create('twitcher_posts').run(self.conn)
            self.r.table('twitcher_posts').index_create('tag').run(self.conn)

    def _unwrap(self, record):
        return {
            'pid': record['pid'],
            'date': record['date'],
            'tag': record['tag'],
            'text': record['text'],
        }

    def add(self, tag, text):
        result = self.r.table('twitcher_posts').insert({
            'pid': '{:d}{:03d}'.format(
                int(time.time() * 1000), random.randint(0, 999)
            ),
            'date': datetime.datetime.utcnow().strftime('%F %T'),
            'tag': tag,
            'text': text,
        }).run(self.conn)
        uid = result['generated_keys'][0]
        return self._unwrap(
            self.r.table('twitcher_posts').get(uid).run(self.conn)
        )

    def list(self, tag=None, before=None, limit=10):
        filters = []
        if tag is not None:
            filters.append(self.r.row['tag'] == tag)
        if before:
            filters.append(self.r.row['pid'] < before)

        filter = self.r.and_(*filters)
        cursor = self.r.table('twitcher_posts').filter(filter) \
            .order_by(self.r.desc('pid')).limit(limit).run(self.conn)
        return [self._unwrap(document) for document in cursor]

    def get(self, pid):
        cursor = self.r.table('twitcher_posts')\
            .filter({'pid': pid}).limit(1).run(self.conn)
        for document in cursor:
            return self._unwrap(document)

    def update(self, pid, tag, text):
        change = {'tag': tag, 'text': text}
        status = self.r.table('twitcher_posts') \
            .filter({'pid': pid}).update(change).run(self.conn)
        return self.get(pid)

    def delete(self, pid):
        status = self.r.table('twitcher_posts') \
            .filter({'pid': pid}).delete().run(self.conn)
        return status['deleted'] == 1

    def get_changes(self, callback):
        import rethinkdb as r
        r.set_loop_type('tornado')

        def inner():
            conn = yield r.connect(**self._params)
            feed = yield r.db(self._params['db']).table('twitcher_posts') \
                .changes().run(conn)
            while (yield feed.fetch_next()):
                change = yield feed.next()
                if change['old_val'] is None:
                    callback(self._unwrap(change['new_val']))
        return inner


def get_storage(dsn):
    """
    :param dsn: Data Source Name ('rethinkdb://admin:@localhost:28015/db')
    :return:
    """
    parsed = urlparse(dsn)
    assert parsed.scheme == 'rethinkdb'
    params = {
        'host': parsed.hostname,
        'port': parsed.port or 28015,
        'db': parsed.path.lstrip('/') or None,
        'user': parsed.username or 'admin',
        'password': parsed.password or None,
    }
    return RethinkdbStorage(params)


if __name__ == '__main__':
    rst = RethinkdbStorage()
    for i in range(10):
        print('>>>', rst.add('t%d' % (i % 3), 'Foo bar'))
        for i in rst.list(tag=None):
            print(i)
        print()
        time.sleep(1)
