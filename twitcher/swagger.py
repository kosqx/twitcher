#!/usr/bin/env python
# -*- coding: utf-8 -*-


import textwrap


def swagger_typedef(typedef):
    if typedef is None:
        return None
    if isinstance(typedef, list):
        return {'items': swagger_typedef(typedef[0]), 'type': 'array'}
    if isinstance(typedef, dict):
        return {'$ref': '#/definitions/%s' % typedef['id']}
    if issubclass(typedef, str):
        return {'type': 'string'}
    if issubclass(typedef, int):
        return {'type': 'integer'}
    if issubclass(typedef, dict):
        return {'type': 'object'}


def swagger_property(name, typedef, **kwargs):
    result = {
        '_name': name.strip('?'),
        '_required': not name.endswith('?'),
    }
    result.update(kwargs)
    result.update(swagger_typedef(typedef))
    return result


def swagger_param(in_, name, typedef, **kwargs):
    result = {
        'in': in_,
        'name': name.strip('?'),
        'required': not name.endswith('?'),

    }
    formatted_typedef = swagger_typedef(typedef)
    if formatted_typedef is not None:
        result['schema'] = formatted_typedef
    result.update(kwargs)
    return result


def swagger_object(name, *properties, **kwargs):
    properties_ = {}
    required_ = []
    for property_ in properties:
        property_name = property_.pop('_name')
        if property_.pop('_required'):
            required_.append(property_name)
        properties_[property_name] = property_
    result = {
        'id': name,
        'properties': properties_,
        'required': required_,
        'type': 'object',
    }
    result.update(kwargs)
    return result


def swagger_endpoint(*params):
    def decorator(method):
        method.swagger_params = params
        method.swagger_data = {
            'parameters': [p for p in params if p['in'] != 'response'],
            'responses': dict(
                (
                    p['name'],
                    dict(
                        (k, v)
                        for k, v in p.items()
                        if k not in ['in', 'name', 'required']
                    )
                )
                for p in params if p['in'] == 'response'
            ),
        }
        return method
    return decorator


def swagger_document(definitions=(), handlers=(), **kwargs):
    result = dict(kwargs)
    result['definitions'] = {}
    for definition in definitions:
        result['definitions'][definition['id']] = definition

    paths = {}
    for handler in handlers:
        path = {}
        for method_name in ['get', 'post', 'put', 'patch', 'delete']:

            method = getattr(handler, method_name, None)
            if method is None or not hasattr(method, 'swagger_data'):
                continue
            doc = textwrap.dedent(getattr(method, '__doc__', '') or '')
            doc_lines = doc.strip().splitlines()
            summary = doc_lines[0] if doc_lines else ''
            description = '\n'.join(doc_lines[1:])

            path[method_name] = {
                'produces': ['application/json'],
                'summary': summary,
                'description': description,
                'operationId': handler.__name__ + '_' + method_name,
                'parameters': [],
                'responses': {},
                'security': [{'basic_auth': []}],
                'tags': ['posts'],
            }
            path[method_name].update(getattr(method, 'swagger_data', {}))
            if method_name in ['post', 'put', 'patch']:
                path[method_name]['consumes'] = ['application/json']

        paths[handler.swagger_path] = path
    result['paths'] = paths

    return result
