# Twitcher

> twitch·er  (twĭch′ər) n.
> 1. One that twitches.
> 2. *Chiefly British* A bird watcher, especially one who observes rare birds.
> 3. A social network from Futurama's episode Attack Of The Killer App


## Installation

```bash
# RethinkDB installation - assumes Ubuntu 16.04
source /etc/lsb-release && echo "deb http://download.rethinkdb.com/apt $DISTRIB_CODENAME main" | sudo tee /etc/apt/sources.list.d/rethinkdb.list
wget -qO- https://download.rethinkdb.com/apt/pubkey.gpg | sudo apt-key add -
sudo apt-get update
sudo apt-get install rethinkdb
sudo cp /etc/rethinkdb/default.conf.sample /etc/rethinkdb/instances.d/instance1.conf
sudo service rethinkdb restart

# Twitcher installation
sudo apt-get install python-pip python-virtualenv
virtualenv --python=python3.5 venv3
git clone https://gitlab.com/kosqx/twitcher.git
source venv3/bin/activate ; cd twitcher ; pip install -r requirements.txt ; cd ..

# starting Twitcher
source venv3/bin/activate ; cd twitcher
TWITCHER_PORT=8000 TWITCHER_DATABASE=rethinkdb://admin:@localhost:28015/twitcher python start.py
```

## Short description

Good:

* Level 2+ on [Richardson Maturity Model](http://martinfowler.com/articles/richardsonMaturityModel.html)
* Swagger UI documentation and interface
* simple AJAX web interface
* updates pushed via WebSockets

Bad:

* no user accounts
* no input data validation
* no security
* no WebSockets on change/update
* only one tag per post
* lots of small bugs (no reconnections, linking all tags, ...)


## Design decisions

* Python
  * learning Golang will take time than allotted
* [Tornado Web Server](http://www.tornadoweb.org/)
  * used in work, wanted to know it better
* WebSockets
  * cool, instant loading of new content
  * performance ()
* [RethinkDB](https://www.rethinkdb.com)
  * read a lot about it, looked cool
  * [scaling, sharding and replication](https://www.rethinkdb.com/docs/sharding-and-replication/)
  * looked realy good at [Distributed Systems Safety Analysis](https://aphyr.com/posts/329-jepsen-rethinkdb-2-1-5), quote: *RethinkDB’s safety claims are accurate*
  * great for WebSockets, quote: *RethinkDB pushes JSON to your apps in realtime*
* [Swagger](https://github.com/swagger-api/swagger-ui)
  * most of code copied from my other project


## Production deploy

Some aspects to take under consideration:

* some concepts from [12-Factor App](http://12factor.net/)
* monitoring, metrics: Graphite/Grafana, optionally Sentry
* log collection: Fluentd/Logstash
* automated deployment

Performance tips:

* guessed load: 1N writes, 100N page display, 1000N update watching
* master for writes, slaves for reading and updates' pushing
* scale by database
  * *one tag per post* allow very simple sharding
  * connecting to shards by AJAX and [CORS](https://en.wikipedia.org/wiki/Cross-origin_resource_sharing)
  * stateless frontend servers = frontend saclability
* if needed WebSockets part can be rewritten into Golang, [allowing for millions of connections](http://goroutines.com/10m)
