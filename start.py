#!/usr/bin/env python
# -*- coding: utf-8 -*-


from twitcher.handlers import start_server


if __name__ == '__main__':
    start_server()
